package edu.uoc.android.restservice.ui.enter;

import android.app.ProgressDialog;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import edu.uoc.android.restservice.rest.model.Followers;
import edu.uoc.android.restservice.rest.model.Owner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoUserActivity extends AppCompatActivity {

    ArrayList<Owner> listaFollowers;
    RecyclerView recyclerViewFollowers;
    TextView textViewRepositories, textViewFollowing;
    ImageView imageViewProfile;
    ProgressDialog progressBar;
    // Se invoca al progress

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_user);

        textViewFollowing = findViewById(R.id.textViewFollowing);
        textViewRepositories = findViewById(R.id.textViewRepositories);
        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);

        listaFollowers = new ArrayList<>();
        recyclerViewFollowers = (RecyclerView)findViewById(R.id.recyclerViewFollowers);

        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));

        String loginName = getIntent().getStringExtra("loginName");
        mostrarDatosBasicos(loginName);
    }

    TextView labelFollowing, labelRepositories, labelFollowers;
    private void initProgressBar()
    {
        textViewFollowing.setVisibility(View.INVISIBLE);
        textViewRepositories.setVisibility(View.INVISIBLE);
        imageViewProfile.setVisibility(View.INVISIBLE);
        recyclerViewFollowers.setVisibility(View.INVISIBLE);

        labelFollowing = (TextView)findViewById(R.id.labelFollowing);
        labelFollowing.setVisibility(View.INVISIBLE);

        labelRepositories = (TextView) findViewById(R.id.labelRepositories);
        labelRepositories.setVisibility(View.INVISIBLE);

        labelFollowers = (TextView) findViewById(R.id.labelFollowers);
        labelFollowers.setVisibility(View.INVISIBLE);

    }

    private void mostrarDatosBasicos(String loginName){
        // La instancia de progressdialog
        progressBar = new ProgressDialog(this);
        // EL Mensaje a mostrar en cuadro de dialogo de  progreso
        progressBar.setMessage("Solicitando Informacion");
        // Mostrar un dialogo de progreso
        progressBar.show();

        GitHubAdapter adapter = new GitHubAdapter();
        Call<Owner> call = adapter.getOwner(loginName);
        call.enqueue(new Callback<Owner>() {

            @Override
            public void onResponse(Call<Owner> call, Response<Owner> response) {
                progressBar.dismiss(); // Finaliza ejecucion de progress
                Owner owner = response.body();

                //La creacion de un if para verificar resultados e enviar datos solicitados
                if (owner != null) {
                    // El numero de repositorios
                    textViewRepositories.setText(owner.getPublicRepos().toString());
                    // El numero de seguidores
                    textViewFollowing.setText(owner.getFollowing().toString());
                    // La imagen del usuario
                    Picasso.get().load(owner.getAvatarUrl()).into(imageViewProfile);
                }else {
                    // si no hay resultados = error
                    Toast.makeText(InfoUserActivity.this, "error existente", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Owner> call, Throwable t) {
                //  Muestra un mensaje de error si falla
                Toast.makeText(InfoUserActivity.this, "existencia de error en la petición de resultados", Toast.LENGTH_SHORT).show();
            }
        });

        // La llamada a la lista de seguidores utilizando su nombre como dato
        Call<List<Followers>> callFollowers = new GitHubAdapter().getOwnerFollowers(loginName);

        callFollowers.enqueue(new Callback<List<Followers>>() {
            @Override
            public void onResponse(Call<List<Followers>> call, Response<List<Followers>> response) {
                // La Sentencia para guardar los datos obtenidos en una lista
                List<Followers> list = response.body();

                // Estabcemos una sentencia if para verificacion de contenido
                if (list != null) {
                    AdaptadorFollowers adapter = new AdaptadorFollowers(list); //envía la lista de seguidores al adapter
                    recyclerViewFollowers.setAdapter(adapter);
                } else {

                    Toast.makeText(InfoUserActivity.this, "Error", Toast.LENGTH_SHORT).show(); //error
                }
            }

            @Override
            public void onFailure(Call<List<Followers>> call, Throwable t) {
                // error en caso de falla
                Toast.makeText(InfoUserActivity.this, "Ha ocurrido un error al realizar la petición", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
